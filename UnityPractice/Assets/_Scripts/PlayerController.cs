﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    Rigidbody rb;
    public float speed;
    int count = 0;
    public Text countText;
    public Text winText;
    public LayerMask groundLayers;
    public float jumpForce = 7;
    public SphereCollider col;
    bool Dead = false;



    // Use this for initialization
    void Start() {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<SphereCollider>();
    }

    // Update is called once per frame
    void Update() {
        if (IsGrounded() && Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            Physics.gravity = new Vector3 (Physics.gravity.magnitude, 0f, 0f);

        }

        if (Dead == false && transform.position.y <= -5)
        {
            Dead = true;
            GetComponent<MeshRenderer>().enabled = false;
            Invoke("RestartLevel", 2f);
        }
    }

    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private bool IsGrounded()
    {
       return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, groundLayers);
    }

    private void FixedUpdate() //This is where to calculate physics things
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        rb.AddForce(movement * Time.deltaTime * speed);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            count += 1;

            SetCountText();

            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("SpeedZone"))
        {
            speed = speed * 2f;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("SpeedZone"))
        {
            speed = speed / 2f;
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();

        if (count >= 12)
        {
            winText.text = "YouWin!";
        }
    }
}
